import java.util.ArrayList;
import java.util.*;

public class Raumschiff {
	   
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	public Raumschiff() {
	}
	
	
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		}
		
	
		public int getPhotonentorpedoAnzahl() {
			return this.photonentorpedoAnzahl;
		}

		public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
			this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		}
		
		
		public int getEnergieversorgungInProzent() {
			return this.energieversorgungInProzent;
		}

		public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
			this.energieversorgungInProzent = energieversorgungInProzent;
		}
		
		
		public int getSchildeInProzent() {
			return this.schildeInProzent;
		}

		public void setSchildeInProzent(int schildeInProzent) {
			this.schildeInProzent = schildeInProzent;
		}
		
		
		public int getHuelleInProzent() {
			return this.huelleInProzent;
		}

		public void setHuelleInProzent(int huelleInProzent) {
			this.huelleInProzent = huelleInProzent;
		}
		
		
		public int getLebenserhaltungssystemeInProzent() {
			return this.lebenserhaltungssystemeInProzent;
		}

		public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
			this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		}
		
		
		public int getAndroidenAnzahl() {
			return this.androidenAnzahl;
		}

		public void setAndroidenAnzahl(int androidenAnzahl) {
			this.androidenAnzahl = androidenAnzahl;
		}
		
		
		public String getSchiffsname() {
			return this.schiffsname;
		}

		public void setSchiffsname(String schiffsname) {
			this.schiffsname = schiffsname;
		}
	
		
		public ArrayList getBroadcastKommunikator() {
			return this.broadcastKommunikator;
		}

		public void setBroadcastKommunikator(ArrayList broadcastKommunikator) {
			this.broadcastKommunikator = broadcastKommunikator;
		}
		
		
		public void addLadung(Ladung neueLadung) {
			this.ladungsverzeichnis.add(neueLadung);
		}
		
		public void photonentorpedoSchiessen(Raumschiff r) {
			if(photonentorpedoAnzahl == 0){ 
				broadcastKommunikator.add("-=*Click*=-");
			}
			else{ 
				photonentorpedoAnzahl -= 1;
				broadcastKommunikator.add("Photonentorpedo abgeschossen");
				this.treffer(r);
			}
		}
		
		public void phaserkanoneSchiessen(Raumschiff r) {
			if(energieversorgungInProzent < 50){ 
				broadcastKommunikator.add("-=*Click*=-");
			}
			else{ 
				energieversorgungInProzent = energieversorgungInProzent - 50;
				broadcastKommunikator.add("Phaserkanone abgeschossen");
				this.treffer(r);
				}
			}
		
		private void treffer(Raumschiff r) {
			System.out.println(r.schiffsname + " wurde getroffen!"); //verbessern in nächste Block
		
			if (r.schildeInProzent > 0) {
				r.schildeInProzent = r.schildeInProzent - 50;
			}
			else if (r.schildeInProzent <= 0) {
				r.huelleInProzent = r.huelleInProzent - 50;
				r.energieversorgungInProzent = r.energieversorgungInProzent - 50;
				if (r.huelleInProzent == 0) {
					r.lebenserhaltungssystemeInProzent = 0;
					broadcastKommunikator.add("Lebenserhaltungssysteme vernichtet");
				}
			}				
		}
		
		//public void nachrichtAnAlle(String message) {
		// }
		
		// "Brauchen Sie nicht machen.": public static ArrayList<String> eintraegeLogbuchZurueckgeben = new ArrayList<String>();
		
		public void photonentorpedosLaden(int anzahlTorpedos) {
			
		}
		
		public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		}
		
		public void zustandRaumschiff() {
			System.out.println("----------------------------------------");
			System.out.println("Anzahl Photonentorpedos: " + photonentorpedoAnzahl);
			System.out.println("Energieversorgung in Prozent: " + energieversorgungInProzent);
			System.out.println("Schilde in Prozent: " + schildeInProzent);
			System.out.println("HülleInProzent: " + huelleInProzent);
			System.out.println("Lebenserhaltungssysteme in Prozent: " + lebenserhaltungssystemeInProzent);
			System.out.println("Anzahl Androiden: " + androidenAnzahl);
			System.out.println("Schiffsname: " + schiffsname);
			System.out.println("----------------------------------------");

		}
		
		public void ladungsverzeichnisAusgeben() {
			System.out.println(ladungsverzeichnis);
		}
		
		public void ladungsverzeichnisAufraeumen() {
			String ladungsbezeichnung = " "; 
			int menge = 0;
			for(int i = 0; i < ladungsverzeichnis.size(); i++) {
				ladungsbezeichnung = ladungsverzeichnis.get(i).getBezeichnung();
				menge = ladungsverzeichnis.get(i).getMenge();
				if(menge == 0){
					ladungsverzeichnis.remove(i);
					i--;
					System.out.println("Die Ladung von " + ladungsbezeichnung + " wurde aus dem Ladungsverzeichnis entfernt.");
				}
			} 
				 }
		
		public void logbuchEintraegeZurueckgeben() {
			System.out.println(broadcastKommunikator);
		}
		
	}