import java.util.Scanner;

public class Fahrkartenautomat {

	public static void main(String[] args) 
	{
		boolean fehler = true;
		while(fehler) {
		double zuZahlen = fahrkartenbestellungErfassen();
    	double bezahlung = fahrkartenBezahlen(zuZahlen);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(bezahlung, zuZahlen);
    	
       // war nicht verlangt (Zusatz)
    	endAusgabe();
		}
    }
    
	
	
	
    
    public static double fahrkartenbestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);
        
        double ticketPreis; 
        int anzahlTickets;
        int auswahlNummer;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag;

        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: \r\n"
        		+ "  Einzelfahrschein Regeltarif AB [3,00 EUR] (1)\r\n"
        		+ "  Tageskarte Regeltarif AB [8,80 EUR] (2)\r\n"
        		+ "  Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR] (3)\r\n");
        
        System.out.println("Ihre Wahl: ");
        
        ticketPreis = tastatur.nextDouble();

        while(ticketPreis <= 0 || ticketPreis > 3) {
            System.out.println(">>Ungültig Eingabe<<");
            System.out.println("Ihre Wahl: ");
            ticketPreis = tastatur.nextDouble();
        }
        
        // AKTUELLE VBB PREISE
        if (ticketPreis == 1 ) {
        	ticketPreis = 3.00;
        }
        else if (ticketPreis == 2) {
        	ticketPreis = 8.80;
        }
        else if (ticketPreis == 3) {
        	ticketPreis = 25.50;
        }
        
        System.out.print("Anzahl der Tickets (Bitte nur Zahlen von 1 - 10 und keine negativen Zahlen): ");
        anzahlTickets = tastatur.nextInt();
        if ((anzahlTickets < 1) || (anzahlTickets > 10)) {
        	anzahlTickets = 1;
            System.out.println("\n!!ACHTUNG!! Sie haben eine ungültige Ticketanzahl ausgewählt. Der Automat rechnet nun mit dem Wert '1' für Sie weiter. \n");
        }
        return ticketPreis = ticketPreis * anzahlTickets;
    }
    
    
    public static double fahrkartenBezahlen(double ticketPreis) {
        Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < ticketPreis)
        {
           System.out.printf("Noch zu zahlen: " + "%.2f", (ticketPreis - eingezahlterGesamtbetrag));
     	   System.out.println(" Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {

        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double ticketPreis) {
    double rückgabebetrag = eingezahlterGesamtbetrag - ticketPreis;
    if(rückgabebetrag > 0.0)
    {
 	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " Euro");
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 Euro-Münzen
        {
     	  System.out.println("2 Euro");
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 Euro-Münzen
        {
     	  System.out.println("1 Euro");
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 Cent-Münzen
        {
     	  System.out.println("50 Cent");
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 Cent-Münzen
        {
     	  System.out.println("20 Cent");
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 Cent-Münzen
        {
     	  System.out.println("10 Cent");
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 Cent-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 0.05;
        }
    }
}
    public static void endAusgabe(){
    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt. \n");
    }

}



/*

1.
	Die Variable "ticketPreis" ist ein double.
	Die Variable "eingezahlterGesamtbetrag" ist ein double.
	Die Variable "eingeworfeneMünze" ist ein double.
	Die Variable "rückgabebetrag" ist ein double.
	Die Variable "anzahlTickets" ist ein integer.
	
2.
	"ticketPreis"
	Operation: einfache Zuweisung, Produkt
	
	"eingezahlterGesamtbetrag"
	Operation: einfache Zuweisung, Vergleich(kleiner als), Additionszuweisung, Differenz
	
	"eingeworfeneMünze"
	Operation: einfache Zuweisung, Additionszuweisung
	
	"rückgabeBetrag"
	Operation: einfache Zuweisung, Vergleich(größer als, größer gleich), Subtraktionszuweisung
	
	"anzahlTickets"
	Operation: einfache Zuweisung
	
5.
	Für den Datentyp "Integer" habe ich mich entschieden, da die Anzahl der Tickets eine Ganzzahl ist. Es gibt keine 1,5 Tickets oder ähnliches.
	Alternativ würde "byte" auch funktionieren, da es sehr unwahrscheinlich ist, mehr als 127 Tickets zu kaufen.

6.
	Der Ausdruck "anzahl * einzelpreis" beschreibt eine Mulitiplaktion zweier Variablen "anzahl" (bei mir "anzahlTickets") und "einzelpreis" (bei mir "Ticketpreis")
	Das heißt in meinem Fall, dass die Variable "ticketPreis" überschrieben wird. Die neue Variable ist das Produkt der Multiplikation  
	von "anzahl" und "einzelpreis". Dies wird getan, um den zu zahlenden Gesamtbetrag herauszufinden.

*/
