
public class HelloWorld {

	public static void main(String[] args) {


		// �bung 1
		
		
		// Aufgabe 1
		
		System.out.print("Das ist ein \"Beispielsatz.\"\n");
		System.out.println("Ein Beispielsatz ist das.");
		
		System.out.println("");
		System.out.println("");
		
		// Aufgabe 2
		
		String s = "*";
		//System.out.print(s);
		
		System.out.printf( "%7s\n", s );
		System.out.printf( "%8s\n", s + s + s );
		System.out.printf( "%9s\n", s + s + s + s + s );
		System.out.printf( "%10s\n", s + s + s + s + s + s + s );
		System.out.printf( "%8s\n", s + s + s);
		System.out.printf( "%8s\n", s + s + s);
		System.out.printf( "%8s\n", s + s + s);
		
		
		System.out.println("");
		System.out.println("");
		
		
		// Aufgabe 3
		
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;

		System.out.printf( "%.2f\n" , a);
		System.out.printf( "%.2f\n" , b);
		System.out.printf( "%.2f\n" , c);
		System.out.printf( "%.2f\n" , d);
		System.out.printf( "%.2f\n" , e);
		
		System.out.println("");
		System.out.println("");
		
		// �bung 2
		
		
		// Aufgabe 1
		
		String k = "*";
		
		System.out.printf( "%5s\n", k + k);
		System.out.printf( k + "%7s\n", k);
		System.out.printf( k + "%7s\n", k);
		System.out.printf( "%5s\n", k + k);

		System.out.println("");
		System.out.println("");
		
		// Aufgabe 2
		
		
		System.out.print("0!");
        System.out.printf("%5s", "=");
        System.out.printf("%19s","");
        System.out.print("=");
        System.out.printf("%5d \n", 1);

        System.out.print("1!");
        System.out.printf("%5s", "=");
        System.out.printf("%-19s"," 1");
        System.out.print("=");
        System.out.printf("%5d \n", 1);

        System.out.print("2!");
        System.out.printf("%5s", "=");
        System.out.printf("%-19s"," 1 * 2");
        System.out.print("=");
        System.out.printf("%5d \n", 2);

        System.out.print("3!");
        System.out.printf("%5s", "=");
        System.out.printf("%-19s"," 1 * 2 * 3");
        System.out.print("=");
        System.out.printf("%5d \n", 6);

        System.out.print("4!");
        System.out.printf("%5s", "=");
        System.out.printf("%-19s"," 1 * 2 * 3 * 4");
        System.out.print("=");
        System.out.printf("%5d \n", 24);

        System.out.print("5!");
        System.out.printf("%5s", "=");
        System.out.printf("%-19s"," 1 * 2 * 3 * 4 * 5");
        System.out.print("=");
        System.out.printf("%5d \n", 120);

		System.out.println("");
		System.out.println("");
	   
		// Arbeitsauftrag-LS05.1-A1.6: AB "Ausgabeformatierung 2"
		
		String far = "Fahrenheit";
        String cel = "Celsius";
        String dot = "------------------------";

        double ff = -28.8889;
		double gg = -23.3333;
		double hh = -17.7778;
		double ii = -6.6667;
		double jj = -1.1111;
        
        int m20 = -20;
		int m10 = -10;
		int pmNu = 0;
		int p20 = 20;
		int p30 = 30;

        System.out.print(far);
        System.out.printf("%3s", "|");
        System.out.printf("%10s \n", cel);
        System.out.println(dot);
        
        System.out.print(m20);
        System.out.printf("%10s", "|");
        System.out.printf("%10.2f \n", ff);
        
        System.out.print(m10);
        System.out.printf("%10s", "|");
        System.out.printf("%10.2f \n", gg);
        
        System.out.print(pmNu);
        System.out.printf("%12s", "|");
        System.out.printf("%10.2f \n", hh);
        
        System.out.print(p20);
        System.out.printf("%11s", "|");
        System.out.printf("%10.2f \n", ii);
        
        System.out.print(p30);
        System.out.printf("%11s", "|");
        System.out.printf("%10.2f \n", jj);
        

//        System.out.printf( "|%-10s|\n", "G�rkem" );
//        System.out.printf( "|%-10s|\n", "18" );
//        System.out.printf( "|%-10s|\n", "75kg" );
//        
//        
//        System.out.printf( "|Name: %-5s| %n|Age: %-21d|Gewicht: %-17s|", "G�rkem", 18, "75kg");

        
	}

}
