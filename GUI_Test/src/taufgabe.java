import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Component;
 
public class taufgabe extends JFrame {

	protected static final Color ColorChooser = null;
	private JPanel contentPane;
	private JTextField txtHierBitteText;
  
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					taufgabe frame = new taufgabe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
   
	/**
	 * Create the frame.
	 */
	public taufgabe() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 316, 650);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		 
		JLabel lblNewLabel = new JLabel("Aufgabe 1");
		lblNewLabel.setBounds(10, 151, 85, 13);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Aufgabe 2");
		lblNewLabel_1.setBounds(10, 234, 85, 13);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Aufgabe 3");
		lblNewLabel_1_1.setBounds(10, 340, 85, 13);
		contentPane.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Aufgabe 4");
		lblNewLabel_1_1_1.setBounds(10, 400, 85, 13);
		contentPane.add(lblNewLabel_1_1_1);
		
		JLabel lblNewLabel_1_1_1_1 = new JLabel("Aufgabe 5");
		lblNewLabel_1_1_1_1.setBounds(10, 448, 67, 13);
		contentPane.add(lblNewLabel_1_1_1_1);
		
		JLabel lblNewLabel_1_1_1_2 = new JLabel("Aufgabe 6");
		lblNewLabel_1_1_1_2.setBounds(10, 510, 85, 13);
		contentPane.add(lblNewLabel_1_1_1_2);
		
		JButton btnNewButton = new JButton("Rot");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.red);
			}
		});
		btnNewButton.setBounds(10, 174, 85, 21);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Gr\u00FCn");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.green);

			}
		});
		btnNewButton_1.setBounds(106, 174, 85, 21);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Blau");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.blue);
			}
		});
		btnNewButton_2.setBounds(201, 174, 85, 21);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Gelb");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.yellow);
			}
		});
		btnNewButton_3.setBounds(10, 199, 85, 21);
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_3_1 = new JButton("Farbe W\u00E4hlen");
		btnNewButton_3_1.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnNewButton_3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(JColorChooser.showDialog(btnNewButton_3_1, getTitle(), ColorChooser));
			}
		});
		btnNewButton_3_1.setBounds(201, 199, 85, 21);
		contentPane.add(btnNewButton_3_1);
		
		JButton btnNewButton_3_2 = new JButton("Standard");
		btnNewButton_3_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(new java.awt.Color(240, 240, 240));
			}
		});
		btnNewButton_3_2.setBounds(106, 199, 85, 21);
		contentPane.add(btnNewButton_3_2);
		
		JLabel lblNewLabel_1_2 = new JLabel("Dieser Text soll ver\u00E4ndert werden");
		lblNewLabel_1_2.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNewLabel_1_2.setBounds(10, 37, 276, 104);
		contentPane.add(lblNewLabel_1_2);
		
		JButton btnNewButton_3_3 = new JButton("Arial");
		btnNewButton_3_3.setActionCommand("");
		btnNewButton_3_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblNewLabel_1_2.getFont().getSize();
				lblNewLabel_1_2.setFont(new Font("Arial", Font.PLAIN, groesse));			
				}
		});
		btnNewButton_3_3.setBounds(10, 255, 85, 21);
		contentPane.add(btnNewButton_3_3);
		
		JButton btnNewButton_3_3_1 = new JButton("Comic Sans TM");
		btnNewButton_3_3_1.setFont(new Font("Tahoma", Font.PLAIN, 7));
		btnNewButton_3_3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblNewLabel_1_2.getFont().getSize();
				lblNewLabel_1_2.setFont(new Font("Comic Sans MS", Font.PLAIN, groesse));			
			}
		});
		btnNewButton_3_3_1.setBounds(106, 255, 85, 21);
		contentPane.add(btnNewButton_3_3_1);
		
		JButton btnNewButton_3_3_1_1 = new JButton("Courier New");
		btnNewButton_3_3_1_1.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_3_3_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblNewLabel_1_2.getFont().getSize();
				lblNewLabel_1_2.setFont(new Font("Courier New", Font.PLAIN, groesse));			
			}
		});
		btnNewButton_3_3_1_1.setBounds(201, 255, 85, 21);
		contentPane.add(btnNewButton_3_3_1_1);
		
		JButton btnNewButton_4 = new JButton("+");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblNewLabel_1_2.getFont().getSize();
				String font = lblNewLabel_1_2.getFont().getFontName();
				lblNewLabel_1_2.setFont(new Font(font, Font.PLAIN, groesse + 1));
			}
		});
		btnNewButton_4.setBounds(10, 417, 135, 21);
		contentPane.add(btnNewButton_4);
		
		JButton btnNewButton_4_1 = new JButton("-");
		btnNewButton_4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblNewLabel_1_2.getFont().getSize();
				String font = lblNewLabel_1_2.getFont().getFontName();
				lblNewLabel_1_2.setFont(new Font(font, Font.PLAIN, groesse - 1));
			}
		});
		btnNewButton_4_1.setBounds(151, 417, 135, 21);
		contentPane.add(btnNewButton_4_1);
		
		JButton btnNewButton_3_3_1_1_1 = new JButton("Schwarz");
		btnNewButton_3_3_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1_2.setForeground(Color.BLACK);
			}
		});
		btnNewButton_3_3_1_1_1.setBounds(201, 366, 85, 21);
		contentPane.add(btnNewButton_3_3_1_1_1);
		
		JButton btnNewButton_3_3_1_2 = new JButton("Blau");
		btnNewButton_3_3_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1_2.setForeground(Color.BLUE);
			}
		});
		btnNewButton_3_3_1_2.setBounds(106, 366, 85, 21);
		contentPane.add(btnNewButton_3_3_1_2);
		
		JButton btnNewButton_3_3_2 = new JButton("Rot");
		btnNewButton_3_3_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1_2.setForeground(Color.RED);	
			}
		});
		btnNewButton_3_3_2.setBounds(10, 366, 85, 21);
		contentPane.add(btnNewButton_3_3_2);
		
		JButton btnNewButton_4_2 = new JButton("EXIT");
		btnNewButton_4_2.setActionCommand("New Button");
		btnNewButton_4_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		        System.exit(1);
			}
		});
		btnNewButton_4_2.setBounds(10, 539, 276, 44);
		contentPane.add(btnNewButton_4_2);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(10, 280, 276, 19);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnNewButton_3_3_2_1 = new JButton("Linksbündig");
		btnNewButton_3_3_2_1.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnNewButton_3_3_2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1_2.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnNewButton_3_3_2_1.setBounds(10, 471, 85, 21);
		contentPane.add(btnNewButton_3_3_2_1);
		
		JButton btnNewButton_3_3_1_2_1 = new JButton("Mittig");
		btnNewButton_3_3_1_2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1_2.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnNewButton_3_3_1_2_1.setBounds(106, 471, 85, 21);
		contentPane.add(btnNewButton_3_3_1_2_1);
		
		JButton btnNewButton_3_3_1_1_1_1 = new JButton("Rechtsbündig");
		btnNewButton_3_3_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnNewButton_3_3_1_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1_2.setHorizontalAlignment(SwingConstants.RIGHT);

			}
		});
		btnNewButton_3_3_1_1_1_1.setBounds(201, 471, 85, 21);
		contentPane.add(btnNewButton_3_3_1_1_1_1);
		
		JButton btnNewButton_4_3 = new JButton("Im Label schreiben");
		btnNewButton_4_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1_2.setText(txtHierBitteText.getText());
			}
		});
		btnNewButton_4_3.setBounds(10, 302, 135, 21);
		contentPane.add(btnNewButton_4_3);
		
		JButton btnNewButton_4_1_1 = new JButton("Vom Label löschen");
		btnNewButton_4_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1_2.setText(" ");

			}
		});
		btnNewButton_4_1_1.setBounds(151, 302, 135, 21);
		contentPane.add(btnNewButton_4_1_1);
		
		
	}
}
