
import java.util.Scanner;

public class Fahrkartenautomat {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		
		   double ticketPreis;
		   int anzahlTickets;
	       double zuZahlenderBetrag; 
	       double eingezahlterGesamtbetrag;
	       double eingeworfeneM�nze;
	       double r�ckgabebetrag;

	       System.out.print("Ticketpreis (Euro): ");
	       ticketPreis = tastatur.nextDouble();
	       
	       System.out.print("Anzahl der Tickets: ");
	       anzahlTickets = tastatur.nextInt();
	       
	       // System.out.print("Zu zahlender Betrag (Euro): ");
	       // zuZahlenderBetrag = tastatur.nextDouble();
	       
	       zuZahlenderBetrag = ticketPreis * anzahlTickets;
	       

	       // Geldeinwurf
	       // -----------
	       eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.println("Noch zu zahlen: " + (String.format("%.2f", zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro"));
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }

	       // Fahrscheinausgabe
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");

	       // R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
	       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	}

}



/*

1.
	Die Variable "ticketPreis" ist ein double.
	Die Variable "eingezahlterGesamtbetrag" ist ein double.
	Die Variable "eingeworfeneM�nze" ist ein double.
	Die Variable "r�ckgabebetrag" ist ein double.
	Die Variable "anzahlTickets" ist ein integer.
	
2.
	"ticketPreis"
	Operation: einfache Zuweisung, Produkt
	
	"eingezahlterGesamtbetrag"
	Operation: einfache Zuweisung, Vergleich(kleiner als), Additionszuweisung, Differenz
	
	"eingeworfeneM�nze"
	Operation: einfache Zuweisung, Additionszuweisung
	
	"r�ckgabeBetrag"
	Operation: einfache Zuweisung, Vergleich(gr��er als, gr��er gleich), Subtraktionszuweisung
	
	"anzahlTickets"
	Operation: einfache Zuweisung
	
5.
	F�r den Datentyp "Integer" habe ich mich entschieden, da die Anzahl der Tickets eine Ganzzahl ist. Es gibt keine 1,5 Tickets oder �hnliches.
	Alternativ w�rde "byte" auch funktionieren, da es sehr unwahrscheinlich ist, mehr als 127 Tickets zu kaufen.

6.
	Der Ausdruck "anzahl * einzelpreis" beschreibt eine Mulitiplaktion zweier Variablen "anzahl" (bei mir "anzahlTickets") und "einzelpreis" (bei mir "Ticketpreis")
	Das hei�t in meinem Fall, dass die Variable "ticketPreis" �berschrieben wird. Die neue Variable ist das Produkt der Multiplikation  
	von "anzahl" und "einzelpreis". Dies wird getan, um den zu zahlenden Gesamtbetrag herauszufinden.

*/


