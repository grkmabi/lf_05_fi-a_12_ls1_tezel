/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/

public class VariablenVerwenden {

	  public static void main(String [] args){
		    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
		          Vereinbaren Sie eine geeignete Variable */
			  int count; 
			  
		    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
		          und geben Sie ihn auf dem Bildschirm aus.*/
			  count = 25;
			  
		    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
		          eines Programms ausgewaehlt werden.
		          Vereinbaren Sie eine geeignete Variable */
			  char menue_point;
			  // count++ // count = count + 1
			  
		    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
		          und geben Sie ihn auf dem Bildschirm aus.*/
			  menue_point = 'C';
			  System.out.println(menue_point);
		    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
		          notwendig.
		          Vereinbaren Sie eine geeignete Variable */
			 final long big_num; // Variable unveränderbar
			 
		    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
		          und geben Sie sie auf dem Bildschirm aus.*/
			  big_num = 3000000l; // in m/s 
			  
		    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
		          soll die Anzahl der Mitglieder erfasst werden.
		          Vereinbaren Sie eine geeignete Variable und initialisieren sie
		          diese sinnvoll.*/
			  int members = 7;
			  
		    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
			  System.out.println(members);

		    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
		          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
		          dem Bildschirm aus.*/
			  double e_charge = (1.602 * (10^(19)));
			  System.out.println(e_charge);


		    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
		          Vereinbaren Sie eine geeignete Variable. */
			  boolean is_payment;
			  
		    /*11. Die Zahlung ist erfolgt.
		          Weisen Sie der Variable den entsprechenden Wert zu
		          und geben Sie die Variable auf dem Bildschirm aus.*/
			  is_payment = true;
			  System.out.println(is_payment);

		  }//main
		}// Variablen
