import java.util.Scanner; // Import der Klasse Scanner
public class Konsoleneingabe2_Alter
{

 public static void main(String[] args) // Hier startet das Programm
 {

 // Neues Scanner-Objekt myScanner wird erstellt
 Scanner myScanner = new Scanner(System.in);

 System.out.print("Hallo. Bitte geben Sie Ihren Namen ein: ");

 // Die Variable zahl1 speichert die erste Eingabe
 String name = myScanner.next();

 System.out.print("Bitte geben Sie Ihr Alter ein: ");

 // Die Variable zahl2 speichert die zweite Eingabe
 int age = myScanner.nextInt();


 System.out.print("Ihr name lautet " + name + " und Sie sind " + age + " Jahre alt. ");

 myScanner.close();

 }
}